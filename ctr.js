$(function () {
    var RANGE = [20, 20];   //座標系の定義域幅
    (function () {
        /* 訓練データに関するDOM要素の生成 */
        var M_t_max = 8;   //選択ボックスに表示する次数の最大値
        var M_t_min = 1;    //選択ボックスに表示する次数の最小値
        createWeightElements($('#tDeg'), $('#tWeight'), "w_t", M_t_max, M_t_min, true);

        /* 学習モデルに関するDOM要素の生成 */
        var M_l_max = 8;   //選択ボックスに表示する次数の最大値
        var M_l_min = 1;    //選択ボックスに表示する次数の最小値
        createWeightElements($('#lDeg'), $('#lWeight'), "w_l", M_l_max, M_l_min, false);

        /* 座標軸の描画 */
        var axesCanvas = $('#axesCanvas')[0];
        drawAxes(axesCanvas, RANGE);
    })();

    var dataRef = {Data:null};  //訓練データの参照
    var fileRef = {file:null};  //訓練データの参照

    var dataCanvas = $('#dataCanvas')[0];   //訓練データ用canvas
    var modelCanvas = $('#modelCanvas')[0]; //学習モデル用canvas
    var COLOR_T = 'rgb(255, 0, 0)';     //訓練データモデル曲線色
    var COLOR_L = 'rgb(0, 0, 255)';     //学習モデル曲線色

    /* イベントハンドラの設定 */
    $('#tDeg').on('change', {weightsSpan: $('#tWeight'), cls: "w_t", initFlg:true}, onWeightDegChanged);
    $('#gen').on('click', {dataRef:dataRef , canvas:dataCanvas, range:RANGE, color:COLOR_T}, onGenerate);
    $('#lDeg').on('change', {weightsSpan: $('#lWeight'), cls: "w_l", initFlg:false}, onWeightDegChanged);
    $('#learn').on('click', {dataRef:dataRef, canvas:modelCanvas, range:RANGE, color:COLOR_L}, onTrain);
    $('#openFile').on('change', {fileRef:fileRef}, onFileOpen);
    $('#genFromFile').on('click', {fileRef:fileRef, dataRef:dataRef, canvas:dataCanvas, range:RANGE, color:COLOR_T}, onGenerateFromFile);
    $('#dataCanvas').on('click', {dataRef:dataRef, range:RANGE}, onCanvasClicked);
    $(window).on('keydown', onEnterkeyDown);
});

function onEnterkeyDown(event) {
    if (event.which === 13) {
        console.log('Enter');
        $('#learn').trigger('click');
    }
}

/* 重みの次元を変更した際のイベントハンドラ */
function onWeightDegChanged(event) {
    var weightsSpan = event.data.weightsSpan;
    weightsSpan.children('span').remove();
    var val = Number($(this).val());
    for (var i=0; i <= val; ++i) {
        var w_input = $('<input>').addClass(event.data.cls).attr('size', '8');
        if (i !== val) {
            w_input.val(0);
        } else {
            if (event.data.initFlg) {
                w_input.val(1);
            } else {
                w_input.val(0);
            }
        }
        var w_span = $('<span>').html('$w_{' + i + '}$:').append(w_input);
        weightsSpan.append(w_span);
    }
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, event.data.id]);
}

/* 重みに関連する動的なDOM要素の生成 */
function createWeightElements(select, span, cls, M_max, M_min, initFlg) {
    for (var i=M_min; i <= M_max; ++i) {
        var option = $('<option>').html(i).val(i);
        select.append(option);
    }
    for (var i=0; i <= M_min; ++i) {
        var childSpan = $('<span>').html("$w_{" + i + "}$:");
        var input = $('<input>').addClass(cls).attr('size', '8');
        if (i !== M_min) {
            input.val(0);
        } else {
            if (initFlg) {
                input.val(1);
            } else {
                input.val(0);
            }
        }
        childSpan.append(input);
        span.append(childSpan);
    }
}

/* 学習開始ボタンを押した際のイベントハンドラ */
function onTrain(event) {
    alert('学習開始');
    var w_l = [];
    var eta = Number($('#eta').val());
    var alpha = Number($('#alpha').val());
    var itr = Number($('#itr').val());
    var Data = event.data.dataRef.Data;
    $(".w_l").each(function(i, elem) {
        w_l[i] = Number($(elem).val());
    });

    var m_l = new Model(phi, w_l);
    var l = new LinearRegression(m_l, eta, alpha, itr);
    l.train(Data);

    var out = $('#output span');
    out.children().remove();
    for (var i=0; i < w_l.length; ++i) {
        var w = m_l.w[i];
        var w_str;
        if (Math.abs(w) >= 1 / 100) {
            w_str = w.toFixed(3);
        } else {
            w_str = w.toExponential(3);
        }
        var htmlText = '$w_{' + i + '} = ' + w_str + '$';
        if (i !== w_l.length - 1) {
            htmlText += ', ';
        }
        var w_span = $('<span>').html(htmlText);
        out.append(w_span);
    }
    var error = l.error(Data);
    var error_str;
    if (error >= 1/ 100) {
        error_str = error.toFixed(3);
    } else if (error === 0) {
        error_str = '0';
    } else {
        error_str = error.toExponential(3);
    }
    var e_span = $('<span>').html('<br/>$error = ' + error_str + '$');
    out.append(e_span);

    MathJax.Hub.Queue(["Typeset", MathJax.Hub]);

    var canvas = event.data.canvas;
    var range = event.data.range;
    var color = event.data.color;
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    drawFunction(m_l, canvas, range, color);
}

/* 訓練データ生成ボタンを押した際のイベントハンドラ */
function onGenerate(event) {
    var sigma = Number($('#sigma').val());
    var N_t = Number($('#N_t').val());
    var w_t = [];
    $(".w_t").each(function(i, elem) {
        w_t[i] = Number($(elem).val());
    });

    var range = event.data.range;
    var m_t = new Model(phi, w_t);
    var trGenerator = new TrainingData(m_t, N_t, sigma, range);
    event.data.dataRef.Data = trGenerator.Data;

    var canvas = event.data.canvas;
    var color = event.data.color;
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    drawPoints(event.data.dataRef.Data, canvas, range);
}

function onCanvasClicked(event) {
    var c = [];
    var canvasRect = event.target.getBoundingClientRect();
    c[0] = event.clientX - canvasRect.left;
    c[1] = event.clientY - canvasRect.top;
    var range = event.data.range;
    var d = canvasToData(c, event.target, range);
    if (event.data.dataRef.Data === null) {
        event.data.dataRef.Data = [];
    }
    event.data.dataRef.Data.push(d);
    drawPoint(d, event.target, range);
}
