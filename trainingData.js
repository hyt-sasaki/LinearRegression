var TrainingData = (function () {
    /* コンストラクタ */
    var TrainingData = function (m, N, s, r) {
        this.model = m;       //訓練データ生成モデル
        this.N = N;       //訓練データ数
        this.sigma = s;     //訓練データ生成時ノイズの分散
        this.range = r;   //座標の定義域幅

        this.generate();    //訓練データの生成
    }

    var pt = TrainingData.prototype;

    /* 訓練データ生成 */
    pt.generate = function() {
        this.Data = [];
        for (var i=0; i < this.N; ++i) {
            var x = Math.random() * this.range[0] - this.range[0] / 2;
            var y = this.model.h(x) + this.gauss(this.sigma);
            //this.T[i] = [x, y];
            this.Data.push({x:x, y:y});
        }
    }

    /* 正規分布ノイズ(Box-Muller法)*/
    pt.gauss = function() {
        x1 = 1 - Math.random();
        x2 = Math.random();

        return Math.sqrt(-2 * Math.log(x1)) * Math.cos(2 * Math.PI * x2) * this.sigma;
    }

    return TrainingData;
})();
