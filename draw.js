/* 点の描画
* d: 点の座標配列
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoint(d, canvas, r) {
    ctx = canvas.getContext('2d');
    var c = dataToCanvas(d, canvas, r);

    ctx.beginPath();
    ctx.arc(c.x, c.y, canvas.width / 125, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.stroke();
}

/* 点群の描画
* ps: 点群の座標配列
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoints(ps, canvas, r) {
    for (var i=0; i < ps.length; ++i) {
        p = ps[i];
        drawPoint(p, canvas, r);
    }
}

/* 関数の描画
* m: 関数のモデル
* canvas: canvasオブジェクト
* r: 座標系の定義幅
* color: 描画する曲線の色
*/
function drawFunction(m, canvas, r, color) {
    ctx = canvas.getContext('2d');
    ctx.strokeStyle = color;

    N = 100;

    ctx.beginPath();
    var x0 = {x:-r[0] / 2, y:m.h(-r[0] / 2)};
    var c0 = dataToCanvas(x0, canvas, r);
    ctx.lineWidth = 3;
    ctx.moveTo(c0.x, c0.y);
    for (var i=1; i <= N; ++i) {
        var x = {x:-r[0] / 2 + r[0] / N * i, y:m.h(-r[0] / 2 + r[0] / N * i)};

        var c = dataToCanvas(x, canvas, r);

        ctx.lineTo(c.x, c.y);
    }
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

/* 座標軸の描画
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawAxes(canvas, r) {
    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    /* x軸の描画 */
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.moveTo(canvas.width, canvas.height / 2);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 53 / 110);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 57 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 105 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 105 / 110, canvas.height * 56 / 110);
    ctx.moveTo(canvas.width * 5 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 5 / 110, canvas.height * 56 / 110);

    /* y軸の描画 */
    ctx.moveTo(canvas.width / 2, canvas.height);
    ctx.lineTo(canvas.width / 2, 0);
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width * 53 / 110, canvas.height * 2 / 110);
    ctx.lineTo(canvas.width * 57 / 110, canvas.height * 2 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 5 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 5 / 110);
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 105 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 105 / 110);

    ctx.stroke();
    ctx.fill();

    /* 文字の描画 */
    var normalSize = Math.min(canvas.width, canvas.height) / 20;
    var fontname = String(normalSize) + "px 'Times New Roman'";
    var fontnameItalic = "Italic " + fontname

    ctx.font = fontnameItalic;
    ctx.textAlign = 'left';
    ctx.textBaseline = 'middle';
    ctx.fillText('y', canvas.width * 58 / 110, canvas.height * 2 / 110);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'middle';
    ctx.font = fontname;
    ctx.fillText(String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 5 / 110);
    ctx.fillText('-' + String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 105 / 110);

    ctx.font = fontnameItalic;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'bottom';
    ctx.fillText('x', canvas.width * 108 / 110, canvas.height * 52 / 110);

    ctx.font = fontname;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'top';
    ctx.fillText(String(r[1] / 2), canvas.width * 105 / 110, canvas.height * 58 / 110);
    ctx.fillText('-' + String(r[1] / 2), canvas.width * 5 / 110, canvas.height * 58 / 110);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'top';
    ctx.fillText('0', canvas.width * 54 / 110, canvas.height * 56 / 110);
}

/* 座標系の変換 */
function dataToCanvas(d, canvas, r) {
    var cx = canvas.width * (1 / 2 + d.x / r[0]);
    var cy = canvas.height * (1 / 2 - d.y / r[1]);
    return {x:cx, y:cy};
}

function canvasToData(c, canvas, r) {
    x = r[0] * (c[0] / canvas.width - 1 / 2);
    y = r[1] * (-c[1] / canvas.height + 1 / 2);
    return {x:x, y:y};
}
