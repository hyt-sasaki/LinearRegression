/* データの生成モデル */
var Model = (function() {
    /* コンストラクタ */
    var Model = function(p, w) {
        this.phi = p;   //基底関数
        this.w = w;     //重み
    }

    var pt = Model.prototype;

    /* モデル関数
       * x: 入力
       */
    pt.h = function(x) {
        var ret = 0;
        for (var i=0; i < this.w.length; ++i) {
            ret += this.w[i] * this.phi(x, i);
        }
        return ret;
    }

    return Model;
})();

/* 多項式基底
* x: x座標
* i: 指数
*/
function phi(x, i) {
    return Math.pow(x, i);
}
