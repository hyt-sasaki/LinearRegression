var LinearRegression = (function () {
    /* コンストラクタ */
    var LinearRegression = function (m, e, a, i) {
        this.model = m; //学習モデル
        this.eta = e; //学習率
        this.alpha = a;   //学習率のスケジューリングファクター
        this.itr = i;   //学習回数
    }

    var pt = LinearRegression.prototype;

    /* 学習 */
    pt.train = function (Data) {
        var choice = [];
        for (var i=0; i < Data.length; ++i) {
            choice[i] = i;
        }

        for (var i=0; i < this.itr; ++i) {
            var c = choice.slice();
            while(c.length > 0) {
                var rnd = Math.floor(Math.random() * c.length);
                d = Data[c[rnd]];
                c.splice(rnd, 1);

                this.update(d);
            }
            this.eta *= this.alpha;
        }
    }

    /* 更新 */
    pt.update = function (d) {
        var x = d.x;
        var y = d.y;
        w_0 = this.model.w.slice(); //重みのコピー
        for (var m=0; m < w_0.length; ++m) {
            w_0[m] += this.eta * (y - this.model.h(x)) * this.model.phi(x, m);
        }
        this.model.w = w_0;
    }

    /* 二乗誤差 */
    pt.error = function (Data) {
        var ret = 0;
        for (var i=0; i < Data.length; ++i) {
            ret += Math.pow(this.model.h(Data[i].x) - Data[i].y, 2);
        }
        ret /= Data.length;
        return ret;
    }

    return LinearRegression;
})();
