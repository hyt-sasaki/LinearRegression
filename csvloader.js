function onFileOpen(event) {
    event.data.fileRef.file = event.target.files[0];
}

function onFileError(event) {
    alert('ファイルの読み込みに失敗しました');
}

function onGenerateFromFile(event) {
    var file = event.data.fileRef.file;
    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function (evt) {
        var csvText = evt.target.result;
        event.data.dataRef.Data = [];
        var lines = csvText.split(/\r\n|\n/);
        for (var i=0; i < lines.length; ++i) {
            var line = lines[i].split(',');
            if (line[0].substr(0, 1) === '#') {
                continue;
            }
            if (line.length >= 2) {
                var data = {x:Number(line[0]), y:Number(line[1])};
                event.data.dataRef.Data.push(data);
            }
        }

        var canvas = event.data.canvas;
        var color = event.data.color;
        var range = event.data.range;
        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
        drawPoints(event.data.dataRef.Data, canvas, range);
        alert('データを読み込みました')
    }
    reader.onerror = onFileError;
}
